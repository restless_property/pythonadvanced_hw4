### **Really dumb 'Flask'-based server**

        This program is a server that processes requests from a client, returning
    pages of the following functionality: 
         - the initial page of greeting
         - an indicator of current date and time
         - a generator of random characters
         - a demonstrator of python libraries in avail
         - a '.csv' processor calculating average values
