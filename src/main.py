"""
This module is a simple server written on 'Flask' library.
The code consists of the following pages:
 - the initial page
 - an indicator of current date and time
 - a generator of random characters
 - a demonstrator of python libraries in avail
 - a '.csv' processor calculating average values
"""

import datetime
#
from flask import Flask, Response
#
import average
import tools

app = Flask(__name__)


@app.route('/')
def initial():
    """
    This function consist of code
    loaded on the initial page.

    :return: (str) a line of salutation
    """
    return '<h1>Welcome to the initial page!</h1>'


@app.route('/now')
def show_now():
    """
    This function calculates
    current date and time of a web-request.

    :return: (str) time and date
                   actual(relevant) at the moment of response
    """
    return str(f"<p style='color:red; font-weight:bold; font-size:200%'>Current date and time: "
               f"{datetime.datetime.now()}</p>")


@app.route('/random')
def get_random():
    """ #
    This function is a generator of random characters
    that differ in relation to the parameters of a request.

    :return result/[Response]: (str) a line of random characters
                               (possibly the hint of an exception)

    Local variables:
    length: (int) the number of characters to output (default: 100)
    digits: (int) the switch of numbers from 0 to 9 to output (default: 0)
    specials: (int) the switch of special characters to output (default: 0)
    """
    try:
        length, digits, specials = tools.validate_random_params()
        result = tools.select_random(quantity=length, numbs=digits, symbs=specials)
    except AttributeError as att_err:
        return Response(str(att_err), status=400)
    return result


@app.route('/requirements')
def get_libs():
    """
    This function returns the list
    of current 'Python' modules in avail.

    :return txt: (str) names of 'Python' libraries

    Local variables:
    file (TextIOWrapper): 'requirements.txt' in 'Python' interpretation
    line (str): one row of text inside the file 'requirements.txt'
    """
    txt = "The file 'requirements.txt' currently contains such libraries:<br/><br/>"
    with open('../requirements.txt') as file:
        for line in file:
            txt += line + "<br/>"
    return txt


@app.route('/csv')
def get_average_csv():
    """
    This function calculates the average
    in weight and height of the subjects
    and computes their quantity.

    :return: (str): calculated results
                    of the measurements

    Local variables:
    data (CSVProcessor): an instance of the class 'CSVProcessor'
                         from the module 'average.py'
    """
    data = average.CSVProcessor('../csv/data.csv')
    data.read_csv()
    data.measure()
    return data.get_data()


# The point of entry.
# Точка входа в программу.
if __name__ == '__main__':
    app.run(debug=True)
