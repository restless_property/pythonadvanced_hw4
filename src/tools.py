"""
This is an auxiliary back-end module that executes various technical tasks.
"""

import random
import string

from flask import request


def validate_random_params():
    """
    THis function validates parameters
    for the function "get_random" of the module "main.py".

    :return (length, digits, specials): (tuple of ints)
                                        values of the parameters
    """
    try:
        length = int(request.args.get('length', random.randint(1, 101)))
        if not (1 <= length <= 1000):
            raise AttributeError("The parameter 'length' "
                                 "accepts only positive integers from 1 to 1000")
        digits = int(request.args.get('digits', 0))
        specials = int(request.args.get('specials', 0))
    except ValueError:
        raise AttributeError('No input or inadmissible characters in the parameters.<br/>'
                             'The parameter "length" only takes positive integers '
                             'in range from 1 to 1000. '
                             'The parameters "digits" and "special" only take '
                             '"0" or "1".')
    if not (0 <= digits <= 1) or not (0 <= specials <= 1):
        raise AttributeError('The parameters "special" and "digits" '
                             'only take "0" or "1" as arguments.')
    return length, digits, specials


def select_random(quantity, numbs, symbs):
    """
    This function forms a line of letters, numbers and special symbols
    dependant on the input parameters of a web request
    and returns randomly picked symbols from this line.

    :param quantity: (int) the number of random symbols
                           to include into the line
    :param numbs: (int) the switch of digits from 0 to 9
                        to include into the line
    :param symbs: (int) the switch of special symbols
                        to include into the line

    :return result: (str) a line of randomly picked symbols

    Local variables:
    compilation: (str) a line of lowercase letters,
                       possibly digits and special symbols,
                       which the random sequence is formed of
    """
    compilation = ''.join(string.ascii_lowercase)
    result = f'<h2></br>Your random line of {quantity} of characters: '
    if numbs == 1:
        # Double concatenation to increase the probability.
        compilation += string.digits + string.digits
    if symbs == 1:
        compilation += string.punctuation
    for lap in range(quantity):
        result += random.choice(compilation)
    return result
